In-Between
==========

A simple implementation of the card game [in-between](https://gamerules.com/rules/in-between-card-game/) in python using tkinter.

I made this game in order to practice using tkinter and also so that I would have a game to play on my [pinephone](https://www.pine64.org/pinephone/).

Liscensing
----------

The [card image with the Tux icon](https://commons.wikimedia.org/wiki/File:Card_back_12.svg) is liscensed under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html).
