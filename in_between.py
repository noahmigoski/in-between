from tkinter import *
from random import randrange
from tkinter import messagebox
import os

root = Tk()

root.title("In Between")

score = 0

# define functions
def restart():
    os.system("python3 in_between.py")
    quit()

def draw():
    if len(cards) > 0:
        card = cards[randrange(0, len(cards))]
        cards.remove(card)
        return card
    else:
        score_label.configure(text="Out of Cards! \n Final score: " + str(score))
        open("scores.csv", "a").write(" " + str(score)) 
        next_btn["state"] = "disabled"
        hit_btn["state"] = "disabled"
        knock_btn["state"] = "disabled"

def card_number(card_obj):
    return int(card_obj.name[1:])

def is_between(middle_card):
   if (card_number(left_card) < card_number(middle_card) < card_number(right_card)) or (card_number(left_card) > card_number(middle_card) > card_number(right_card)):
        return True
   else:
        return False

def knock():
    global score
    new_card = draw()
    middle.configure(image=new_card)
    if is_between(new_card):
        score -= 1
    else:
        score += 1
    score_label.configure(text="Score: " + str(score))
    knock_btn["state"] = "disabled"
    hit_btn["state"] = "disabled"
    next_btn["state"] = "normal"

def hit():
    global score
    new_card = draw()
    middle.configure(image=new_card)
    if is_between(new_card):
        score += 1
    else:
        score -= 1
    score_label.configure(text="Score: " + str(score))
    hit_btn["state"] = "disabled"
    knock_btn["state"] = "disabled"
    next_btn["state"] = "normal"

def next():
    global left_card
    global right_card
    left_card = draw()
    right_card = draw()
    middle.configure(image=back)
    left.configure(image=left_card)
    right.configure(image=right_card)
    if (card_number(left_card) == card_number(right_card)) or (card_number(left_card) == card_number(right_card) - 1) or (card_number(left_card) == card_number(right_card) + 1):
        hit_btn["state"] = "disabled"
        knock_btn["state"] = "disabled"
        next_btn["state"] = "normal"
    else:
        hit_btn["state"] = "normal"
        knock_btn["state"] = "normal"
        next_btn["state"] = "disabled"

def readFile(filename):
    fileObj = open(filename, "r")
    entries = fileObj.readlines()
    fileObj.close()
    return entries

def viewScores():
    scores = readFile('scores.csv')[0].split()
    formatted_scores = []
    for i in scores:
        formatted_scores.append(int(i))
    formatted_scores.sort()
    formatted_scores.reverse()
    messagebox.showinfo('High Scores', str(formatted_scores[:3]))

# Menu

menu = Menu(root)
new_item = Menu(menu, tearoff=0)
new_item.add_command(label='New Game', command=restart)
new_item.add_command(label='View Highscores', command=viewScores)

menu.add_cascade(label='File', menu=new_item)
root.config(menu=menu)
 
# Specify Grid
Grid.rowconfigure(root,0,weight=1)
Grid.columnconfigure(root,0,weight=1)
 
Grid.rowconfigure(root,1,weight=1)
Grid.columnconfigure(root,1,weight=1)

Grid.rowconfigure(root,2,weight=1)
Grid.columnconfigure(root,2,weight=1)
 
Grid.rowconfigure(root,3,weight=1)
Grid.columnconfigure(root,3,weight=1)


# Create Buttons
knock_btn = Button(root,text="Knock",command=knock)
hit_btn = Button(root,text="Hit",command=hit)
next_btn = Button(root,text="Next",command=next)

hit_btn["state"] = "disabled"
knock_btn["state"] = "disabled"

# Create Card Images

back = PhotoImage(file='images/back.png')
C1 = PhotoImage(file='images/1C.png', name='C1')
D1 = PhotoImage(file='images/1D.png', name='D1')
H1 = PhotoImage(file='images/1H.png', name='H1')
S1 = PhotoImage(file='images/1S.png', name='S1')

C2 = PhotoImage(file='images/2C.png', name='C2')
D2 = PhotoImage(file='images/2D.png', name='D2')
H2 = PhotoImage(file='images/2H.png', name='H2')
S2 = PhotoImage(file='images/2S.png', name='S2')

C3 = PhotoImage(file='images/3C.png', name='C3')
D3 = PhotoImage(file='images/3D.png', name='D3')
H3 = PhotoImage(file='images/3H.png', name='H3')
S3 = PhotoImage(file='images/3S.png', name='S3')

C4 = PhotoImage(file='images/4C.png', name='C4')
D4 = PhotoImage(file='images/4D.png', name='D4')
H4 = PhotoImage(file='images/4H.png', name='H4')
S4 = PhotoImage(file='images/4S.png', name='S4')

C5 = PhotoImage(file='images/5C.png', name='C5')
D5 = PhotoImage(file='images/5D.png', name='D5')
H5 = PhotoImage(file='images/5H.png', name='H5')
S5 = PhotoImage(file='images/5S.png', name='S5')

C6 = PhotoImage(file='images/6C.png', name='C6')
D6 = PhotoImage(file='images/6D.png', name='D6')
H6 = PhotoImage(file='images/6H.png', name='H6')
S6 = PhotoImage(file='images/6S.png', name='S6')

C7 = PhotoImage(file='images/7C.png', name='C7')
D7 = PhotoImage(file='images/7D.png', name='D7')
H7 = PhotoImage(file='images/7H.png', name='H7')
S7 = PhotoImage(file='images/7S.png', name='S7')

C8 = PhotoImage(file='images/8C.png', name='C8')
D8 = PhotoImage(file='images/8D.png', name='D8')
H8 = PhotoImage(file='images/8H.png', name='H8')
S8 = PhotoImage(file='images/8S.png', name='S8')

C9 = PhotoImage(file='images/9C.png', name='C9')
D9 = PhotoImage(file='images/9D.png', name='D9')
H9 = PhotoImage(file='images/9H.png', name='H9')
S9 = PhotoImage(file='images/9S.png', name='S9')

C10 = PhotoImage(file='images/10C.png', name='C10')
D10 = PhotoImage(file='images/10D.png', name='D10')   
H10 = PhotoImage(file='images/10H.png', name='H10')
S10 = PhotoImage(file='images/10S.png', name='S10')

C11 = PhotoImage(file='images/11C.png', name='C11')   
D11 = PhotoImage(file='images/11D.png', name='D11')   
H11 = PhotoImage(file='images/11H.png', name='H11')   
S11 = PhotoImage(file='images/11S.png', name='S11')   

C12 = PhotoImage(file='images/12C.png', name='C12')   
D12 = PhotoImage(file='images/12D.png', name='D12')   
H12 = PhotoImage(file='images/12H.png', name='H12')   
S12 = PhotoImage(file='images/12S.png', name='S12')   

C13 = PhotoImage(file='images/13C.png', name='C13')   
D13 = PhotoImage(file='images/13D.png', name='D13')   
H13 = PhotoImage(file='images/13H.png', name='H13')   
S13 = PhotoImage(file='images/13S.png', name='S13')   

cards = [D1, C1, H1, S1,
         D2, C2, H2, S2,
         D3, C3, H3, S3,
         D4, C4, H4, S4,
         D5, C5, H5, S5,
         D6, C6, H6, S6,
         D7, C7, H7, S7,
         D8, C8, H8, S8,
         D9, C9, H9, S9,
         D10, C10, H10, S10,
         D11, C11, H11, S11,
         D12, C12, H12, S12,
         D13, C13, H13, S13]

# smaller image example
img1 = D10.subsample(2, 2)   

# Create Label
score_label = Label(root, text="Score: " + str(score), font=("Arial Bold", 16)) 
left = Label(root, image=back)
middle = Label(root, image=back) 
right = Label(root, image=back) 
 
# Set grid
hit_btn.grid(row=2,column=1,sticky="NSEW")
knock_btn.grid(row=2,column=0,sticky="NSEW")
next_btn.grid(row=2,column=2,sticky="NSEW")
score_label.grid(row=0,column=0,sticky="NSEW")
left.grid(row=1,column=0,sticky="NSEW")
middle.grid(row=1,column=1,sticky="NSEW")
right.grid(row=1,column=2,sticky="NSEW")

 
# Execute tkinter
root.mainloop()
